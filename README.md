# Test Chat

## RUN

    npm run

## DIR

    ./src
        ./redux
        ./constants
        ./config
        ./components
        ./request
        ./styles
        app,js
        store.js

## GIT

[repository](https://bitbucket.org/gdayramirez/proyecttest/src/master/)

## DEMO

[URL](https://chattest-85f79.firebaseapp.com)
