import React from "react";
import { Images } from "../../constants/images";

export const ChatHeader = props => {
  return (
    <header>
      <div className="title">
        <p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
      </div>
      <div className="meta">
        <a href="#" className="author">
          <span className="name">Picky Rick</span>
          <img src={Images[1]} alt="" />
        </a>
      </div>
    </header>
  );
};
